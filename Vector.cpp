#include "Vector.h"
#include <malloc.h>
#include <iostream>

using namespace std;

Vector::Vector(void) : m_nbElement(0),tailleDouble(0),tailleInt(0)
{
	this->m_vector = new String[1] ;
	m_vector[0].set(0,'B');
    m_nbElement++;
	

}
int Vector::nbElement() 
{
	return m_nbElement;
}
void Vector::add(String element) 
{
	if(m_nbElement == 1 && m_vector[0].get(0) == 'B')
	{
		m_echange = new String[m_nbElement];
		m_echange[m_nbElement-1] = element ;
		this->m_vector = new String[m_nbElement];
		for(int i=0; i<m_nbElement ;i++)
			m_vector[i] = m_echange[i] ;
		delete [] m_echange;
	}
	else
	{
		m_echange = new String[m_nbElement+1] ; // allouer une case pour le nouveau element
		
		for(int i = 0 ; i<m_nbElement ; i++)
		m_echange[i] = m_vector[i];

		m_echange[m_nbElement] = element ;
		this->m_vector = new String[m_nbElement +1];
		for(int i=0; i<m_nbElement+1 ;i++)
			m_vector[i] = m_echange[i] ;
		delete [] m_echange;
		m_nbElement ++;

	}
	
	
	
} 
String Vector::getString(int indice)
{
	return (m_vector[indice]);

	
}


void Vector::add(Vector &elements) 
{
	int size = m_nbElement + elements.nbElement() ;
	{
	cout << "etape 1 : " << endl; 
	m_echange = new String[size]; 
	for(int i = 0; i<m_nbElement ; i++)
		m_echange[i] = m_vector[i] ; 
	cout << "etape 2 : " << endl; 
	for(int i = m_nbElement,j=0 ; i<size ; i++, j++)
		m_echange[i] = elements.getString(j) ;
	cout << "etape 3 : " << endl; 
	this->m_vector = new String[size] ;
	for(int i=0 ; i<size ; i++)
		m_vector[i] = m_echange[i] ; 

	delete[] m_echange ;
	m_nbElement = size ;
	cout << "nb element : " << m_nbElement << endl;
	}
}
void Vector::print()
{
	for(int i=0; i < this->nbElement() ; i++)
	{
		cout << "V[" << i << "] = " ; 
		m_vector[i].affiche();
		cout << endl;
	}
}
String Vector::get(int index) 
{
	return (this->getString(index)) ;
}
void Vector::set(int index, String element) 
{
	this->m_vector[index] = element ;
}
String Vector::remove(int index) 
{
	m_echange = new String[m_nbElement-1] ;
	for(int i=index+1,j=0;i<m_nbElement;i++,j++)
		m_echange[j] = m_vector[i] ;

	this->m_vector = new String[m_nbElement-1];
		for(int i=0;i<m_nbElement-1;i++)
		m_vector[i] = m_echange[i] ;
	String s1;
	s1 = m_echange[index];
	delete [] m_echange;
	m_nbElement--;
	return s1;

}
double* Vector::getDoubleVector() 
{
	int size = this->nbElement();
	double *tabDouble;
	int tailleTab = 0;
	tabDouble = (double*)malloc((tailleTab)*sizeof(double));
	int j =0;
	for(int i=0;i<m_nbElement;i++)
	{
		if(m_vector[i].testDouble())
		{ 
			tailleTab++;
			tabDouble = (double*)realloc(tabDouble,tailleTab*sizeof(double));
			tabDouble[j] = m_vector[i].doubleValue();
			j++;
			
		}
	}
	this->tailleDouble = tailleDouble ;
	return tabDouble;
}
int* Vector::getIntVector() 
{
	int size = this->nbElement();
	int tailletab = 0;
	int *tabInt;
	tabInt = (int*)malloc(tailleInt*sizeof(int));
	int j =0;
	for(int i=0;i<m_nbElement;i++)
	{
		if(m_vector[i].testInt())
		{
			tailletab++;
			tabInt = (int*)realloc(tabInt,tailletab*sizeof(int));
			tabInt[j] = m_vector[i].intValue();
			j++;
		}
	}
	this->tailleInt = size;
	return tabInt;
}
int Vector::gettailleDouble()
{
	return this->tailleDouble;
}
int Vector::gettailleInt()
{
	return this->tailleInt;
}
Vector::~Vector(void)
{
	delete [] m_vector;
	m_nbElement--;
}
