#pragma once
#include "String.h"
class Vector : public String
{
public:
	Vector(void);
	
	void add(String element); 
	void add(Vector &elements);
	String getString(int num) ;
	void print(); 
	String get(int index); 
	void set(int index, String element) ; 
	String remove(int index); 
	double* getDoubleVector() ;
	int* getIntVector();
	int gettailleInt(); 
	int gettailleDouble();  
	int nbElement(); 
	~Vector(void); 
private : 
	String *m_vector;
	String *m_echange;
	int tailleInt;
	int tailleDouble;
    int m_nbElement ;
	
};
