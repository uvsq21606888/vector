#pragma once
class String
{
public:
	String(); 
	String(int size);
	String(char *value); 
	String(double i); 
	String(char a); 
	String(String &string); //constructeur de copie
	int lenght(); 
	char get(int i); 
	void set(int i, char c); // remplace le caractere numero i par c 
	void set(char *string); 
	void set(String string); 
	void append(char *string); 
	void toUpper(); 
	void toLower() ; 
	void insert(int i, char *string); 
	void affiche();
	void insert(int i, String string) ; 
	String substring(int i, int j); 
	int pos(char *string); 
	bool existeChaine(char *value,String string,int i);
	bool existeChaine(char *value,char *string,int i); 
	int pos(String string); 
	int intDecimal(char i);
	int intValue(); 
	double doubleValue();
    String operator=(String  a); // surcharge de l'operator + pour le type String
	bool testDouble();
	bool testInt(); 
	~String();
private:
	char *m_value;

};
